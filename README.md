# ttp-ex3-css-intro

## Введение с CSS

### Виды селекторов CSS

- селекторы тегов
- селекторы классов
- селекторы идентификаторов
- контекстные селекторы
- смежные селекторы
- сестринские селекторы
## Пример

https://mesothelial-speeder.000webhostapp.com/

## Полезные ссылки

- Pipeline validator [https://bitbucket-pipelines.prod.public.atl-paas.net/validator]
- Команда npm run [https://frontender.info/task_automation_with_npm_run/]

